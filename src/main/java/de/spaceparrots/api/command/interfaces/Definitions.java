package de.spaceparrots.api.command.interfaces;

/**
 * <h1>CommandAPI Remastered</h1>
 *
 * @author Julius Korweck
 * @version 05.12.2018
 * @since 05.12.2018
 */
public interface Definitions
{

    static Definitions standard()
    {
        return new Definitions() {};
    }

    /**
     * Marker for optional argument tags.<br>
     * pattern: /command --tag1 value --tag2 value
     */
    default String getArgumentTagMarker()
    {
        return "--";
    }

    /**
     * Marker for the beginning and the name of a command line.<br>
     * pattern: /command
     */
    default String getCommandMarker()
    {
        return "/";
    }

    /**
     * Marker for a variable to make inter-command communication possible.<br>
     * pattern: /command1 > $var1 | /command2 $var1 > $var2 | /command3 $var1 $var2
     */
    default String getVariableMarker()
    {
        return "$";
    }

    /**
     * Marker for concatenation of multiple commands. They will be executed left-to-right.<br>
     * pattern: /command1 | /command2 | /command3
     */
    default String getCommandConcatenator()
    {
        return "|";
    }

    /**
     * Parentheses symbol for string constants to allow empty space in string arguments.<br>
     * Note that real parentheses like {@code (} wont be recognized to use {@code )} for the ending of the string constant.<br>
     * pattern: /command "argument in parentheses" "another one"
     */
    default String getStringConstantParentheses()
    {
        return "\"";
    }

    /**
     * Marker for directing resulting values into variables.<br>
     * pattern: /command > $res
     */
    default String getResultingMarker()
    {
        return ">";
    }

    /**
     * Marker for deeply accessing variables to get specific properties from their values.<br>
     * pattern: /command $var1.property1 $var1.property2
     */
    default String getDeepAccessMarker()
    {
        return ".";
    }

    /**
     * Marker for abstractly accessing variables to get them as variables or as their types.<br>
     * Without this marker, the variable refers to its value.
     * A single abstraction marker refers to the variable as variable
     * and two markers refer to the type of the variable/the type of the value the variable is referring to.
     * pattern: /command $var1? $var1??
     */
    default String getAbstractAccessMarker()
    {
        return "?";
    }

    /**
     * Marker for type casting a variable value during the resolving process.<br>
     * Usually needed when accessing the deep access marker for getting a properties value,
     * but the current type of the variable does not support it.<br>
     * The type cast marker is followed by the name of the type to be casted to.<br>
     * pattern: /command $var1:char-array.length
     */
    default String getTypeCastMarker()
    {
        return ":";
    }

}

/***********************************************************************************************
 *
 *                  All rights reserved, SpaceParrots UG (c) copyright 2018
 *
 ***********************************************************************************************/