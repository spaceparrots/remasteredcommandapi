package de.spaceparrots.api.command.interfaces;

/**
 * <h1>CommandAPI Remastered</h1>
 *
 * @author Julius Korweck
 * @version 14.12.2018
 * @since 14.12.2018
 */
@FunctionalInterface
public interface ImplicitData
{

    String implicitData( int id );

    default boolean isExecutionPermitted( String permission )
    {
        return false;
    }

    default ImplicitData addValue( int id, String value )
    {
        ImplicitData prev = this;
        return new ImplicitData()
        {
            @Override
            public String implicitData( int x )
            {
                return id == x ? value : prev.implicitData( x );
            }

            @Override
            public boolean isExecutionPermitted( String permission )
            {
                return prev.isExecutionPermitted( permission );
            }
        };
    }

}

/***********************************************************************************************
 *
 *                  All rights reserved, SpaceParrots UG (c) copyright 2018
 *
 ***********************************************************************************************/