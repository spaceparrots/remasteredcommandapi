package de.spaceparrots.api.command.interfaces;

import de.spaceparrots.api.type.Type;

import java.util.ArrayList;
import java.util.Set;

/**
 * <h1>CommandAPI Remastered</h1>
 *
 * @author Julius Korweck
 * @version 18.12.2018
 * @since 18.12.2018
 */
public interface CommandObject<Result>
{

    String getName();

    Type<Result> getResultingType();

    String getPermission();

    void setPermission( String permission );

    boolean hasPermission();

    ArrayList<Parameter> getParameters();

    Set<CommandObject<?>> getJuniorCommands();

    boolean hasJuniorsCommands();

}

/***********************************************************************************************
 *
 *                  All rights reserved, SpaceParrots UG (c) copyright 2018
 *
 ***********************************************************************************************/