package de.spaceparrots.api.command.interfaces;

import de.spaceparrots.api.command.exception.UnknownPropertyException;
import de.spaceparrots.api.command.exception.UnknownTypeException;
import de.spaceparrots.api.type.Type;
import de.spaceparrots.api.type.TypeContainer;

/**
 * <h1>CommandAPI Remastered</h1>
 *
 * @author Julius Korweck
 * @version 15.12.2018
 * @since 15.12.2018
 */
@FunctionalInterface
public interface FunctionStack<T, R>
{

    R apply( T t );

    default FunctionStack<T, Type<R>> abstraction( TypeContainer types )
    {
        return t -> {
            R apply = apply( t );
            Type<R> type = types.getType( (Class<R>) apply.getClass() );
            if ( type == null )
                throw new UnknownTypeException( "There is no type for class \"" + apply.getClass().getTypeName() + "\"" );
            return type;
        };
    }

    default FunctionStack<T, ?> access( TypeContainer types, String property )
    {
        return t ->
        {
            R apply = apply( t );
            Type<R> type = types.getType( (Class<R>) apply.getClass() );
            if ( type == null )
                throw new UnknownTypeException( "There is no type for class \"" + apply.getClass().getTypeName() + "\"" );
            if ( type.hasProperty( property ) )
            {
                Type<?> propertyType = type.getPropertyType( property );
                return type.resolvePropertyAs( apply, property, propertyType );
            }
            else
            {
                throw new UnknownPropertyException( "There is no such property in type \"" + type.name() + "\": " + property );
            }
        };
    }

    default <S> FunctionStack<T, S> format( TypeContainer types, Type<S> targetFormat )
    {
        return t ->
        {
            R apply = apply( t );
            Type<R> type = types.getType( (Class<R>) apply.getClass() );
            if ( type == null )
                throw new UnknownTypeException( "There is no type for class \"" + apply.getClass().getTypeName() + "\"" );
            return targetFormat.deepResolve( type, apply, remember -> false );
        };
    }

}

/***********************************************************************************************
 *
 *                  All rights reserved, SpaceParrots UG (c) copyright 2018
 *
 ***********************************************************************************************/