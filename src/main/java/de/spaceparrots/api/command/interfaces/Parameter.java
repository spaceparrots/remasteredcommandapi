package de.spaceparrots.api.command.interfaces;

import de.spaceparrots.api.type.Type;

/**
 * <h1>CommandAPI Remastered</h1>
 *
 * @author Julius Korweck
 * @version 05.12.2018
 * @see Type
 * @since 05.12.2018
 */
public interface Parameter
{

    Integer SCOPE_DEFAULT = 0;

    /**
     * The type
     */
    Type<?> type();

    /**
     * The name
     */
    String name();

    /**
     * Whether this parameter is optional for calling the command
     */
    boolean isOptional();

    /**
     * Whether this parameter is an input argument or a scoped inherited command execution value.<br>
     *
     * @return the inherited command execution value
     *
     * @see this#hasScope()
     */
    int scope();

    /**
     * this parameter is an input argument or a scoped inherited command execution value.
     */
    boolean hasScope();

}

/***********************************************************************************************
 *
 *                  All rights reserved, SpaceParrots UG (c) copyright 2018
 *
 ***********************************************************************************************/