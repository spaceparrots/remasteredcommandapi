package de.spaceparrots.api.command.interfaces;

import de.spaceparrots.api.command.core.ECommandResult;

/**
 * <h1>CommandAPI Remastered</h1>
 *
 * @author Julius Korweck
 * @version 07.12.2018
 * @since 07.12.2018
 */
public interface CommandResult
{

    static CommandResult create( Object value, ECommandResult result, String message )
    {
        return new CommandResult()
        {
            @Override
            public Object get()
            {
                return value;
            }

            @Override
            public ECommandResult getResult()
            {
                return result;
            }

            @Override
            public String getResultMessage()
            {
                return message;
            }
        };
    }

    Object get();

    ECommandResult getResult();

    String getResultMessage();

    default boolean wasSuccessful()
    {
        return getResult() == ECommandResult.SUCCESS;
    }

}

/***********************************************************************************************
 *
 *                  All rights reserved, SpaceParrots UG (c) copyright 2018
 *
 ***********************************************************************************************/