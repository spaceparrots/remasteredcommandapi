package de.spaceparrots.api.command.interfaces;

/**
 * <h1>CommandAPI Remastered</h1>
 *
 * @author Julius Korweck
 * @version 20.12.2018
 * @since 20.12.2018
 */
public interface Resolver<Source, T>
{

}

/***********************************************************************************************
 *
 *                  All rights reserved, SpaceParrots UG (c) copyright 2018
 *
 ***********************************************************************************************/