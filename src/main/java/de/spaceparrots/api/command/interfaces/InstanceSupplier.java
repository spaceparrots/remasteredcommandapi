package de.spaceparrots.api.command.interfaces;

import java.lang.reflect.Constructor;
import java.util.HashSet;
import java.util.Set;
import java.util.function.Supplier;

/**
 * <h1>CommandAPI Remastered</h1>
 *
 * @author Julius Korweck
 * @version 07.12.2018
 * @since 07.12.2018
 */
public interface InstanceSupplier
{

    static InstanceSupplier create()
    {
        return new InstanceSupplier()
        {
            @Override
            public <T> T produce( Class<T> clazz )
            {
                try
                {
                    Constructor<T> constructor = clazz.getConstructor();
                    if ( constructor != null )
                    {
                        if ( !constructor.isAccessible() )
                            constructor.setAccessible( true );
                        return constructor.newInstance();
                    }
                }
                catch ( Exception e )
                {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            public Set<Class<?>> supports()
            {
                return new HashSet<>();
            }
        };
    }

    /**
     * Contains all supported classes of this supplier
     *
     * @return a set of all supported classes
     */
    Set<Class<?>> supports();

    /**
     * Produces a new instance for the given class using the construction method defined in the supplier.
     * This method might throw exceptions although there are not tagged to the method.
     *
     * @param clazz the class to create an instance of
     * @param <T>   the type to find the supplier and match the result
     *
     * @return a new instance for the given class or null if the class is unknown or couldn't be created
     */
    <T> T produce( Class<T> clazz );

    default <T> InstanceSupplier addAlternative( Class<T> producerClass, Supplier<T> producer )
    {
        InstanceSupplier prev = this;
        return new InstanceSupplier()
        {
            @Override
            public <X> X produce( Class<X> clazz )
            {
                if ( clazz.equals( producerClass ) )
                    return clazz.cast( producer.get() );
                else return prev.produce( clazz );
            }

            @Override
            public Set<Class<?>> supports()
            {
                Set<Class<?>> type = prev.supports();
                type.add( producerClass );
                return type;
            }
        };
    }

}

/***********************************************************************************************
 *
 *                  All rights reserved, SpaceParrots UG (c) copyright 2018
 *
 ***********************************************************************************************/