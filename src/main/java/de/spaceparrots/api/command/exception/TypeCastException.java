package de.spaceparrots.api.command.exception;

/**
 * <h1>CommandAPI Remastered</h1>
 *
 * @author Julius Korweck
 * @version 07.12.2018
 * @since 07.12.2018
 */
public class TypeCastException extends CommandAPIException
{

    public TypeCastException( ClassCastException cause, String method, String className )
    {
        super( "An error occured casting the result of \"" + method + "\" in \"" + className + "\": " + cause.toString() );
    }

}

/***********************************************************************************************
 *
 *                  All rights reserved, SpaceParrots UG (c) copyright 2018
 *
 ***********************************************************************************************/