package de.spaceparrots.api.command.exception;

/**
 * <h1>CommandAPI Remastered</h1>
 *
 * @author Julius Korweck
 * @version 15.12.2018
 * @since 15.12.2018
 */
public final class UnknownPropertyException extends CommandAPIException
{

    public UnknownPropertyException( String message )
    {
        super( message );
    }

}

/***********************************************************************************************
 *
 *                  All rights reserved, SpaceParrots UG (c) copyright 2018
 *
 ***********************************************************************************************/