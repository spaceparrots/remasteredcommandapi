package de.spaceparrots.api.command.exception;

/**
 * <h1>CommandAPI Remastered</h1>
 *
 * @author Julius Korweck
 * @version 07.12.2018
 * @since 07.12.2018
 */
public final class InnerCommandExecutionException extends CommandAPIException
{

    public InnerCommandExecutionException( Exception cause, String method, String className, String parameters )
    {
        super( "An error occured while executing \"" + method + "\" in \"" + className + "\" with parameters: " + parameters, cause );
    }

}

/***********************************************************************************************
 *
 *                  All rights reserved, SpaceParrots UG (c) copyright 2018
 *
 ***********************************************************************************************/