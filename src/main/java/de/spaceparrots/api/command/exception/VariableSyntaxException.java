package de.spaceparrots.api.command.exception;

/**
 * <h1>CommandAPI Remastered</h1>
 *
 * @author Julius Korweck
 * @version 17.12.2018
 * @since 17.12.2018
 */
public final class VariableSyntaxException extends CommandAPIException
{

    public VariableSyntaxException( String message )
    {
        super( message );
    }

}

/***********************************************************************************************
 *
 *                  All rights reserved, SpaceParrots UG (c) copyright 2018
 *
 ***********************************************************************************************/