package de.spaceparrots.api.command.exception;

/**
 * <h1>CommandAPI Remastered</h1>
 *
 * @author Julius Korweck
 * @version 07.12.2018
 * @since 07.12.2018
 */
class CommandAPIException extends RuntimeException
{

    CommandAPIException()
    {

    }

    CommandAPIException( String message )
    {
        super( message );
    }

    CommandAPIException( String message, Exception cause )
    {
        super( message, cause );
    }

}

/***********************************************************************************************
 *
 *                  All rights reserved, SpaceParrots UG (c) copyright 2018
 *
 ***********************************************************************************************/