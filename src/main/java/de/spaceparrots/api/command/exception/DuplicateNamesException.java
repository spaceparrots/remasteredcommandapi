package de.spaceparrots.api.command.exception;

/**
 * <h1>CommandAPI Remastered</h1>
 *
 * @author Julius Korweck
 * @version 18.12.2018
 * @since 18.12.2018
 */
public final class DuplicateNamesException extends CommandAPIException
{

    public DuplicateNamesException( String message )
    {
        super( message );
    }

}

/***********************************************************************************************
 *
 *                  All rights reserved, SpaceParrots UG (c) copyright 2018
 *
 ***********************************************************************************************/