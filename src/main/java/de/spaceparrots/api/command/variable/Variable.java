package de.spaceparrots.api.command.variable;

import de.spaceparrots.api.type.Type;

/**
 * <h1>RemasteredCommandAPI</h1>
 *
 * @author Julius Korweck
 * @version 08.12.2018
 * @since 08.12.2018
 */
public class Variable<T>
{

    private Type<T> type;
    private T value;

    public Variable( Type<T> type )
    {
        this.type = type;
    }

    public Variable( Type<T> type, T value )
    {
        this( type );
        this.value = value;
    }

    public Type<T> getType()
    {
        return type;
    }

    public <R> Variable<R> transform( Type<R> type )
    {
        R resolve = null;
        if ( type.canResolve( this.type ) )
            resolve = type.resolve( this.type, value );
        else
            resolve = type.deepResolve( this.type, value, x -> false );
        return resolve == null ? null : new Variable<>( type, resolve );
    }

    public T getValue()
    {
        return value;
    }

    public void setValue( T value )
    {
        this.value = value;
    }

}
/***********************************************************************************************
 *
 *                  All rights reserved, SpaceParrots UG (c) copyright 2018
 *
 ***********************************************************************************************/