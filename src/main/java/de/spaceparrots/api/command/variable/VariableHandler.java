package de.spaceparrots.api.command.variable;

import de.spaceparrots.api.type.Type;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

/**
 * <h1>CommandAPI Remastered</h1>
 *
 * @author Julius Korweck
 * @version 09.12.2018
 * @since 09.12.2018
 */
public final class VariableHandler
{

    private HashMap<String, Variable<?>> variables;

    public VariableHandler()
    {
        this.variables = new HashMap<>();
    }

    public Variable<?> getVariable( String name )
    {
        return variables.get( name );
    }

    public boolean hasVariable( String name )
    {
        return variables.containsKey( name );
    }

    public <T> boolean hasVariable( Type<T> type, T value )
    {
        Optional<Map.Entry<String, Variable<?>>> any = variables.entrySet().stream()
                .filter( entry -> entry.getValue().getType().equals( type ) )
                .filter( entry -> entry.getValue().getValue().equals( value ) )
                .findAny();
        return any.isPresent();
    }

    public <T> Variable<T> getVariable( Type<T> type, T value )
    {
        Optional<Map.Entry<String, Variable<?>>> any = variables.entrySet().stream()
                .filter( entry -> entry.getValue().getType().equals( type ) )
                .filter( entry -> entry.getValue().getValue().equals( value ) )
                .findAny();
        return any.isPresent() ? (Variable<T>) any.get().getValue() : null;
    }

    public <T> String getVariableName( Type<T> type, T value )
    {
        Optional<Map.Entry<String, Variable<?>>> any = variables.entrySet().stream()
                .filter( entry -> entry.getValue().getType().equals( type ) )
                .filter( entry -> entry.getValue().getValue().equals( value ) )
                .findAny();
        return any.isPresent() ? any.get().getKey() : null;
    }

    public <T> void introduceVariable( String name, Type<T> type, T value )
    {
        this.variables.put( name, new Variable<>( type, value ) );
    }

    public <T> Variable<T> getVariableAs( String name, Type<T> type )
    {
        Variable<?> variable = getVariable( name );
        if ( variable != null )
        {
            return variable.transform( type );
        }
        else return null;
    }

}

/***********************************************************************************************
 *
 *                  All rights reserved, SpaceParrots UG (c) copyright 2018
 *
 ***********************************************************************************************/