package de.spaceparrots.api.command.util;

/**
 * <h1>CommandAPI Remastered</h1>
 *
 * @author Julius Korweck
 * @version 20.12.2018
 * @since 20.12.2018
 */
public class CommandAPIConstants
{

    public static final int SCOPE_DEFAULT = 0; //Zero
    public static final int ORIGINAL_INPUT = -1;
    public static final int ORIGINAL_WITHOUT_PROCESSED = -2;
    public static final int ORIGINAL_COMMANDLINE = -3;
    public static final int ORIGINAL_COMMAND_NAME = -4;

}

/***********************************************************************************************
 *
 *                  All rights reserved, SpaceParrots UG (c) copyright 2018
 *
 ***********************************************************************************************/