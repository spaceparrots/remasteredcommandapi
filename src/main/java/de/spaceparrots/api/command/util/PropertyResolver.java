package de.spaceparrots.api.command.util;

import de.spaceparrots.api.command.interfaces.Resolver;

import java.util.function.Function;

/**
 * <h1>CommandAPI Remastered</h1>
 *
 * @author Julius Korweck
 * @version 20.12.2018
 * @since 20.12.2018
 */
public final class PropertyResolver<T, Property> implements Resolver<Property, T>
{

    private String name;
    private Function<T, Property> function;
    private final Class<Property> targetClass;

    PropertyResolver( String name, Class<Property> targetClass, Function<T, Property> apply )
    {
        this.name = name;
        this.function = apply;
        this.targetClass = targetClass;
    }

    public Property apply( T s )
    {
        return function.apply( s );
    }

    public Class<Property> targetClass()
    {
        return targetClass;
    }

    public String name()
    {
        return name;
    }

    public static <A, B> PropertyResolver<A, B> create( String name, Class<B> target, Function<A, B> apply )
    {
        return new PropertyResolver<>( name, target, apply );
    }

}

/***********************************************************************************************
 *
 *                  All rights reserved, SpaceParrots UG (c) copyright 2018
 *
 ***********************************************************************************************/