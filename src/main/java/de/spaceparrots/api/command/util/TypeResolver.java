package de.spaceparrots.api.command.util;

import de.spaceparrots.api.command.interfaces.Resolver;

import java.util.function.Function;
import java.util.function.Supplier;

/**
 * <h1>CommandAPI Remastered</h1>
 *
 * @author Julius Korweck
 * @version 07.12.2018
 * @since 07.12.2018
 */
public class TypeResolver<Source, T> implements Resolver<Source, T>
{

    private Function<Source, T> function;
    private Function<T, Source> function2;
    private final Class<Source> sourceClass;

    TypeResolver( Class<Source> source, Function<Source, T> apply, Function<T, Source> apply2 )
    {
        this.function = apply;
        this.function2 = apply2;
        this.sourceClass = source;
    }

    public T apply( Source s )
    {
        return function.apply( s );
    }

    public Source applyBack( T t )
    {
        return function2.apply( t );
    }

    public Class<Source> sourceClass()
    {
        return sourceClass;
    }

    public static <A, B> TypeResolver<A, B> create( Class<A> source, Function<A, B> apply, Function<B, A> apply2 )
    {
        return new TypeResolver<>( source, apply, apply2 );
    }

    public static <A> TypeResolver<String, A> simple( Supplier<A> supplier )
    {
        return create( String.class, x -> supplier.get(), x -> "" );
    }

}

/***********************************************************************************************
 *
 *                  All rights reserved, SpaceParrots UG (c) copyright 2018
 *
 ***********************************************************************************************/