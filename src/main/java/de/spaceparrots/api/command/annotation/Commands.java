package de.spaceparrots.api.command.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * <h1>CommandAPI Remastered</h1>
 * Repeatable annotation array of {@link Command}.
 *
 * @author Julius Korweck
 * @version 06.12.2018
 * @since 06.12.2018
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface Commands
{

    Command[] value();

}

/***********************************************************************************************
 *
 *                  All rights reserved, SpaceParrots UG (c) copyright 2018
 *
 ***********************************************************************************************/