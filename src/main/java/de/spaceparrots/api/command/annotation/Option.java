package de.spaceparrots.api.command.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * <h1>CommandAPI Remastered</h1>
 * This annotation defines the tagged parameter as optional to the command.<br>
 * The annotation {@link Arg} will be overwritten by {@link Option}.
 *
 * @author Julius Korweck
 * @version 03.12.2018
 * @see Arg
 * @since 03.12.2018
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.PARAMETER)
public @interface Option
{

    /**
     * Optional name.
     *
     * @return the name of the tag for the optional parameter
     */
    String value();

}

/***********************************************************************************************
 *
 *                  All rights reserved, SpaceParrots UG (c) copyright 2018
 *
 ***********************************************************************************************/