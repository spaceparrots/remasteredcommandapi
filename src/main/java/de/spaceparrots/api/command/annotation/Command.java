package de.spaceparrots.api.command.annotation;

import java.lang.annotation.*;

/**
 * <h1>CommandAPI Remastered</h1>
 *
 * @author Julius Korweck
 * @version 06.12.2018
 * @since 03.12.2018
 */
@Repeatable(Commands.class)
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface Command
{

    /**
     * The name representing this command
     */
    String value();

}

/***********************************************************************************************
 *
 *                  All rights reserved, SpaceParrots UG (c) copyright 2018
 *
 ***********************************************************************************************/