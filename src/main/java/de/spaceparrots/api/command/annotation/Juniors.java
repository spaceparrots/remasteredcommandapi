package de.spaceparrots.api.command.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * <h1>CommandAPI Remastered</h1>
 * Repeatable annotation array of {@link Junior}.
 *
 * @author Julius Korweck
 * @version 05.12.2018
 * @see Junior
 * @since 05.12.2018
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface Juniors
{

    Junior[] value();

}

/***********************************************************************************************
 *
 *                  All rights reserved, SpaceParrots UG (c) copyright 2018
 *
 ***********************************************************************************************/