package de.spaceparrots.api.command.annotation;

import java.lang.annotation.*;

/**
 * <h1>CommandAPI Remastered</h1>
 * Must not be tagged to a method without the {@link Command} annotation.
 * This annotation is repeatable.
 *
 * @author Julius Korweck
 * @version 05.12.2018
 * @see Command
 * @see Senior
 * @since 05.12.2018
 */
@Repeatable(Juniors.class)
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface Junior
{

    /**
     * A class containing methods tagged with {@link Command} to be considered ad subordinated commands to the tagged one.<br>
     * This clazz might not be tagged with {@link Senior} or this {@link Junior} will be ignored.
     */
    Class<?> value();

}

/***********************************************************************************************
 *
 *                  All rights reserved, SpaceParrots UG (c) copyright 2018
 *
 ***********************************************************************************************/