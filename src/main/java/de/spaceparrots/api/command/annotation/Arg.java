package de.spaceparrots.api.command.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * <h1>CommandAPI Remastered</h1><br>
 * <p>
 * A tagged parameter will get a new name for handling in this api.<br>
 * If its missing, the java name of this parameter is used instead; so this annotation is not necessary, just a help.<br>
 *
 * @author Julius Korweck
 * @version 03.12.2018
 * @see Option
 * @since 15.07.2017
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.PARAMETER)
public @interface Arg
{

    /**
     * Attaches a name to a method parameter; overwrites the name in the java code in the command api system.<br>
     * Primarily used for the help page of this command, but its used for identification purposes too.
     *
     * @return the new name for the tagged parameter
     */
    String value();

}

/***********************************************************************************************
 *
 *                  All rights reserved, SpaceParrots UG (c) copyright 2018
 *
 ***********************************************************************************************/