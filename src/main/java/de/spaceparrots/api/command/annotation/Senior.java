package de.spaceparrots.api.command.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * <h1>CommandAPI Remastered</h1>
 * Must be tagged to a clazz containing method with the {@link Command} annotation
 * to make all of them be considered as top-level commands. <br>
 * The tagged clazz can no longer be used in {@link Junior}.
 *
 * @author Julius Korweck
 * @version 07.12.2018
 * @see Junior
 * @since 07.12.2018
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface Senior
{

}

/***********************************************************************************************
 *
 *                  All rights reserved, SpaceParrots UG (c) copyright 2018
 *
 ***********************************************************************************************/