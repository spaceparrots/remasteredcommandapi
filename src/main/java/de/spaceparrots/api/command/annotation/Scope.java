package de.spaceparrots.api.command.annotation;

import de.spaceparrots.api.command.util.CommandAPIConstants;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * <h1>CommandAPI Remastered</h1>
 * This annotation is for tagging parameter in @Command tagged methods.
 * Will mark a parameter to be replaced with an implicit value when calling the command.
 *
 * @author Julius Korweck
 * @version 03.12.2018
 * @since 04.03.2017
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.PARAMETER)
public @interface Scope
{

    /**
     * The value representing the scoped value of the resolving method.<br>
     * Everything below zero belongs to intern data, zero itself is default and positive values are unclaimed.
     */
    int value() default CommandAPIConstants.SCOPE_DEFAULT;

}

/***********************************************************************************************
 *
 *                  All rights reserved, SpaceParrots UG (c) copyright 2018
 *
 ***********************************************************************************************/
