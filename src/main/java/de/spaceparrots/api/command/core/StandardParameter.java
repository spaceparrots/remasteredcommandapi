package de.spaceparrots.api.command.core;

import de.spaceparrots.api.command.interfaces.Parameter;
import de.spaceparrots.api.type.Type;

/**
 * <h1>CommandAPI Remastered</h1>
 *
 * @author Julius Korweck
 * @version 05.12.2018
 * @since 05.12.2018
 */
final class StandardParameter implements Parameter
{

    private final Type<?> type;
    private final String name;
    private final int scope;
    private final boolean hasScope;

    StandardParameter( Type<?> type, String name, int scope, boolean hasScope )
    {
        this.type = type;
        this.name = name;
        this.scope = scope;
        this.hasScope = hasScope;
    }

    @Override
    public Type<?> type()
    {
        return type;
    }

    @Override
    public String name()
    {
        return name;
    }

    @Override
    public boolean isOptional()
    {
        return false;
    }

    @Override
    public int scope()
    {
        return scope;
    }

    @Override
    public boolean hasScope()
    {
        return hasScope;
    }

}

/***********************************************************************************************
 *
 *                  All rights reserved, SpaceParrots UG (c) copyright 2018
 *
 ***********************************************************************************************/