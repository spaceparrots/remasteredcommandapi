package de.spaceparrots.api.command.core;

import de.spaceparrots.api.command.exception.InnerCommandExecutionException;
import de.spaceparrots.api.command.exception.TypeCastException;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.function.Function;

/**
 * <h1>CommandAPI Remastered</h1>
 *
 * @author Julius Korweck
 * @version 07.12.2018
 * @since 07.12.2018
 */
class CommandExecution<T> implements Function<Object[], T>
{

    private Object executingBody;
    private Method executionMethod;

    CommandExecution( Object executingBody, Method executionMethod )
    {
        this.executingBody = executingBody;
        this.executionMethod = executionMethod;
    }

    @Override
    public T apply( Object[] objects )
    {
        Object result;
        try
        {
            //directly calling the method by using the executingBody and the given parameters
            //parameter permutation for the correct should be done at this point
            result = executionMethod.invoke( executingBody, objects );
        }
        catch ( Exception e )
        {
            //any kind of exception will be thrown here as RuntimeException after some details are added to it
            throw new InnerCommandExecutionException( e, executionMethod.getName(),
                    executingBody.getClass().getCanonicalName(), objects != null ? Arrays.toString( objects ) : "null" );
        }
        T typedResult;
        try
        {
            //trying to cast the result of the method to the given class type
            typedResult = (T) result;
        }
        catch ( ClassCastException e )
        {
            //reducing the exception output and adding some information to it
            throw new TypeCastException( e, executionMethod.getName(), executingBody.getClass().getCanonicalName() );
        }
        return typedResult;
    }

}

/***********************************************************************************************
 *
 *                  All rights reserved, SpaceParrots UG (c) copyright 2018
 *
 ***********************************************************************************************/