package de.spaceparrots.api.command.core;

/**
 * <h1>CommandAPI Remastered</h1>
 *
 * @author Julius Korweck
 * @version 07.12.2018
 * @since 07.12.2018
 */
public enum ECommandResult
{

    SUCCESS,
    PROCESSOR_NOT_READY,
    UNPERMITTED,
    INVALID_ARGUMENTS,
    SYNTAX_EXCEPTION,
    EXECUTION_EXCEPTION;

}

/***********************************************************************************************
 *
 *                  All rights reserved, SpaceParrots UG (c) copyright 2018
 *
 ***********************************************************************************************/