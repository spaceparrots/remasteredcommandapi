package de.spaceparrots.api.command.core;

/**
 * <h1>CommandAPI Remastered</h1>
 *
 * @author Julius Korweck
 * @version 11.12.2018
 * @since 11.12.2018
 */
class DataPair<A, B>
{

    private A a;
    private B b;

    public DataPair( A a, B b )
    {
        this.a = a;
        this.b = b;
    }

    public A getA()
    {
        return a;
    }

    public void setA( A a )
    {
        this.a = a;
    }

    public B getB()
    {
        return b;
    }

    public void setB( B b )
    {
        this.b = b;
    }

}

/***********************************************************************************************
 *
 *                  All rights reserved, SpaceParrots UG (c) copyright 2018
 *
 ***********************************************************************************************/