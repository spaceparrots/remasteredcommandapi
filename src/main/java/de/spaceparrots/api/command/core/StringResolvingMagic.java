package de.spaceparrots.api.command.core;

import de.spaceparrots.api.command.interfaces.Definitions;
import de.spaceparrots.api.command.variable.VariableHandler;
import de.spaceparrots.api.type.Type;

import java.util.ArrayList;
import java.util.List;
import java.util.function.BiFunction;
import java.util.stream.Collectors;

/**
 * <h1>CommandAPI Remastered</h1>
 *
 * @author Julius Korweck
 * @version 11.12.2018
 * @since 11.12.2018
 */
final class StringResolvingMagic
{

    private Type<String> stringType;
    private Definitions definitions;

    public StringResolvingMagic( Type<String> stringType, Definitions definitions )
    {
        this.stringType = stringType;
        this.definitions = definitions;
    }

    private String reduceByStringConstants( String commandLine, String resVar, VariableHandler handler )
    {
        //find next unassigned temp var name
        int var = 0;
        String varName = "temp";
        while ( handler.hasVariable( varName + var )
                || commandLine.contains( definitions.getVariableMarker() + varName + var )
                || resVar.equals( varName + var ) )
        {
            var++;
        }
        varName += var;

        int current = 0;
        String content = null;
        //find next unreplaced string constant
        while ( current < commandLine.length() )
        {
            char c = commandLine.charAt( current );
            //String constant begin or end
            if ( c == '"' )
            {
                //no content -> string constant beginning
                if ( content == null )
                {
                    content = "";
                }
                else //content already read -> string constant ending
                {
                    //replace found string constant with a new temp variable and recursive call this method with the new line
                    //if there is already a variable with the exact same content, use the existing one
                    if ( !handler.hasVariable( stringType, content ) )
                        handler.introduceVariable( varName, stringType, content );
                    else varName = handler.getVariableName( stringType, content );
                    String pre, succ;
                    pre = commandLine.substring( 0, commandLine.indexOf( definitions.getStringConstantParentheses() ) );
                    succ = commandLine.substring( current + 1 );
                    return reduceByStringConstants( pre + definitions.getVariableMarker() + varName + succ, resVar, handler );
                }
            }
            else
            {
                if ( content != null )
                    content += c;
            }
            current++;
        }
        return commandLine;
    }

    private String[][] splitCommandLine( String commandLine )
    {
        String[] split = commandLine.split( " " );
        List<String[]> argList = new ArrayList<>();
        List<String> argsList = new ArrayList<>();
        int curr = 1;

        //command name
        argList.add( new String[] { split[0] } );

        while ( curr < split.length )
        {
            if ( split[curr].startsWith( definitions.getArgumentTagMarker() ) )
            {
                argList.add( new String[] { split[curr], split[curr + 1] } );
                curr += 2;
            }
            else
            {
                argsList.add( split[curr] );
                curr++;
            }
        }
        argList.addAll( 1, argsList.stream().map( s -> new String[] { s } ).collect( Collectors.toList() ) );
        return argList.toArray( new String[argList.size()][] );
    }

    BiFunction<String, VariableHandler, DataPair<String[][], String>> singleCommandHandler()
    {
        return this::handleSingleCommand;
    }

    private DataPair<String[][], String> handleSingleCommand( String command, VariableHandler handler )
    {
        String commandLine, resultVariable = "";
        //devide string into result variable and result variable prefix (the command itself)
        int i = command.lastIndexOf( definitions.getResultingMarker() );
        if ( i > 0 && i < command.length() )
        {
            commandLine = command.substring( 0, i ).trim();
            resultVariable = command.substring( i + 1 ).trim();
            if ( !resultVariable.startsWith( definitions.getVariableMarker() ) )
            {
                commandLine += definitions.getResultingMarker() + resultVariable;
                resultVariable = "";
            }
            else
            {
                //remove variable marker from name
                resultVariable = resultVariable.substring( definitions.getVariableMarker().length() );
            }
        }
        else commandLine = command.trim();
        //reduce by string constants by making them to variables
        commandLine = reduceByStringConstants( commandLine, resultVariable, handler );
        //split commandLine into command name, and argument pairs while sorting optional arguments to the end
        String[][] splitCommandLine = splitCommandLine( commandLine );
        return new DataPair<>( splitCommandLine, resultVariable );
    }

}

/***********************************************************************************************
 *
 *                  All rights reserved, SpaceParrots UG (c) copyright 2018
 *
 ***********************************************************************************************/