package de.spaceparrots.api.command.core;

import de.spaceparrots.api.command.interfaces.CommandObject;
import de.spaceparrots.api.command.interfaces.Parameter;
import de.spaceparrots.api.type.Type;

import java.util.ArrayList;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * <h1>CommandAPI Remastered</h1>
 *
 * @author Julius Korweck
 * @version 06.12.2018
 * @since 05.12.2018
 */
final class ResolvedCommand<Result> implements CommandObject<Result>
{

    private final Function<Object[], Result> function;

    private final String name;
    private final Type<Result> resultingType;

    private final ArrayList<Parameter> parameters;
    private final Set<ResolvedCommand<?>> juniors;

    private String permission;

    ResolvedCommand( String name, Type<Result> resultingType, ArrayList<Parameter> parameters,
                     Function<Object[], Result> function )
    {
        this( name, resultingType, null, parameters, function );
    }

    ResolvedCommand( String name, Type<Result> resultingType, Set<ResolvedCommand<?>> juniors,
                     ArrayList<Parameter> parameters, Function<Object[], Result> function )
    {
        this.name = name;
        this.resultingType = resultingType;
        this.juniors = juniors;
        this.parameters = parameters;
        this.function = function;
    }

    Function<Object[], Result> getFunction()
    {
        return function;
    }

    public Type<Result> getResultingType()
    {
        return resultingType;
    }

    public String getPermission()
    {
        return permission;
    }

    public void setPermission( String permission )
    {
        this.permission = permission;
    }

    public boolean hasPermission()
    {
        return permission != null;
    }

    public String getName()
    {
        return name;
    }

    public ArrayList<Parameter> getParameters()
    {
        return parameters;
    }

    Set<ResolvedCommand<?>> getJuniors()
    {
        return juniors;
    }

    public Set<CommandObject<?>> getJuniorCommands()
    {
        return getJuniors().stream().map( x -> (CommandObject<?>) x ).collect( Collectors.toSet() );
    }

    public boolean hasJuniorsCommands()
    {
        return this.juniors != null && !this.juniors.isEmpty();
    }

}

/***********************************************************************************************
 *
 *                  All rights reserved, SpaceParrots UG (c) copyright 2018
 *
 ***********************************************************************************************/