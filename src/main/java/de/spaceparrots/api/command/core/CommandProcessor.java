package de.spaceparrots.api.command.core;

import de.spaceparrots.api.command.annotation.Senior;
import de.spaceparrots.api.command.exception.DuplicateNamesException;
import de.spaceparrots.api.command.exception.UnknownTypeException;
import de.spaceparrots.api.command.exception.VariableSyntaxException;
import de.spaceparrots.api.command.interfaces.*;
import de.spaceparrots.api.command.util.CommandAPIConstants;
import de.spaceparrots.api.command.util.PropertyResolver;
import de.spaceparrots.api.command.util.TypeResolver;
import de.spaceparrots.api.command.variable.Variable;
import de.spaceparrots.api.command.variable.VariableHandler;
import de.spaceparrots.api.type.Type;
import de.spaceparrots.api.type.TypeContainer;

import java.util.*;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * <h1>CommandAPI Remastered</h1>
 *
 * @author Julius Korweck
 * @version 18.12.2018
 * @since 05.12.2018
 */
public final class CommandProcessor
{

    private Definitions definitions;
    private TypeContainer typeContainer;
    private InstanceSupplier instanceSupplier;
    private Set<ResolvedCommand<?>> commands; //will initialized in enable method
    private StringResolvingMagic stringResolvingMagic; //will be initialized in enable method bcs of dependencies
    private ImplicitData defaultImplicit;

    private boolean enabled;

    public CommandProcessor( Definitions definitions )
    {
        this.definitions = definitions;
        this.typeContainer = TypeContainer.create();
        this.instanceSupplier = InstanceSupplier.create();
        this.enabled = false;
    }

    public <S, T> CommandProcessor registerType( Class<T> typeClass, String typeName, Resolver<?, T>... resolvers )
    {
        if ( !enabled )
        {
            Type<T> type = this.typeContainer.introduceType( typeClass, typeName );
            Arrays.stream( resolvers ).forEach( f ->
            {
                //type resolver
                if ( f instanceof TypeResolver )
                {
                    TypeResolver<S, T> res = (TypeResolver<S, T>) f;
                    Class<S> sourceClass = res.sourceClass();
                    Type<S> sType = typeContainer.getType( sourceClass );
                    type.introduceResolver( sType, res::apply );
                    sType.introduceResolver( type, res::applyBack );
                } //property resolver
                else if ( f instanceof PropertyResolver )
                {
                    PropertyResolver<T, S> res = (PropertyResolver<T, S>) f;
                    Class<S> targetClass = res.targetClass();
                    Type<S> sType = typeContainer.getType( targetClass );
                    type.introducePropertyResolver( res.name(), sType, res::apply );
                }
            } );
        }
        return this;
    }

    public <T> CommandProcessor register( Class<T> clazz, Supplier<T> supplier )
    {
        if ( !enabled )
            this.instanceSupplier = instanceSupplier.addAlternative( clazz, supplier );
        return this;
    }

    public <T> CommandProcessor register( T... objects )
    {
        if ( !enabled )
        {
            for ( T object : objects )
            {
                Class<T> clazz = (Class<T>) object.getClass();
                this.instanceSupplier = instanceSupplier.addAlternative( clazz, () -> object );
            }
        }
        return this;
    }

    public void setDefaultImplicit( ImplicitData implicit )
    {
        if ( !enabled )
            this.defaultImplicit = implicit;
    }

    public Set<CommandObject<?>> getCommands()
    {
        if ( enabled )
            return this.commands.stream().map( x -> (CommandObject<?>) x ).collect( Collectors.toSet() );
        return null;
    }

    public TypeContainer getTypeContainer()
    {
        if ( !enabled )
            return typeContainer;
        else return null;
    }

    public void enable()
    {

        if ( enabled ) return;

        //resolve all known class and instances of classes to ResolvedCommand objects
        CommandResolver resolver = new CommandResolver( typeContainer, instanceSupplier );
        //all classes supported by the instance supplier
        Set<Class<?>> supports = instanceSupplier.supports();
        //resolves all commands contained in the instance supplier
        this.commands = supports.stream()
                .filter( c -> c.isAnnotationPresent( Senior.class ) ) //only classes which are senior command classes
                .map( resolver::resolveCommands )
                .flatMap( Set::stream )
                .collect( Collectors.toSet() );

        //validating name and searches for missing types
        if ( validateCommands( this.commands ) )
        {
            this.stringResolvingMagic = new StringResolvingMagic( typeContainer.getType( String.class ), definitions );
            this.enabled = true;
        }

    }

    public Function<String, CommandResult> simpleCommandResolveHandler()
    {
        return simpleInputHandler_( false );
    }

    public Function<String, CommandResult> simpleInputHandler()
    {
        return simpleInputHandler_( true );
    }

    public BiFunction<String, ImplicitData, CommandResult> commandResolveHandler()
    {
        return inputHandler_( false );
    }

    public BiFunction<String, ImplicitData, CommandResult> inputHandler()
    {
        return inputHandler_( true );
    }

    private Function<String, CommandResult> simpleInputHandler_( boolean execute )
    {
        Function<String, CommandResult> function = s -> CommandResult.create( null, ECommandResult.PROCESSOR_NOT_READY,
                "The processor has to be enabled to accept input data" );
        Function<String, CommandResult> function2 = s -> CommandResult.create( null, ECommandResult.PROCESSOR_NOT_READY,
                "The processor does not contain a default implicit value gatherer" );
        Function<String, CommandResult> simpleInputHandler = s -> enabled ? ( defaultImplicit == null ? function2.apply( s ) : this.onInput( s, defaultImplicit, execute ) ) : function.apply( s );
        return input ->
        {
            try
            {
                return simpleInputHandler.apply( input );
            }
            catch ( Exception e )
            {
                return CommandResult.create( e, ECommandResult.EXECUTION_EXCEPTION, e.getMessage() );
            }
        };
    }

    public BiFunction<String, ImplicitData, CommandResult> inputHandler_( boolean execute )
    {
        Function<String, CommandResult> function = s -> CommandResult.create( null, ECommandResult.PROCESSOR_NOT_READY,
                "The processor has to be enabled to accept input data" );
        BiFunction<String, ImplicitData, CommandResult> inputHandler = ( s, i ) -> enabled ? this.onInput( s, i, execute ) : function.apply( s );
        return ( input, implicit ) ->
        {
            try
            {
                return inputHandler.apply( input, implicit );
            }
            catch ( Exception e )
            {
                return CommandResult.create( e, ECommandResult.EXECUTION_EXCEPTION, e.getMessage() );
            }
        };
    }

    private synchronized CommandResult onInput( String input, ImplicitData implicitData, boolean execute )
    {
        if ( input == null )
            return CommandResult.create( null, ECommandResult.INVALID_ARGUMENTS, "Input is null" );
        if ( implicitData == null )
            return CommandResult.create( null, ECommandResult.INVALID_ARGUMENTS, "Implicit Value Gatherer is null" );
        VariableHandler handler = new VariableHandler(); //VariableHandler for this command call
        String concatenator = definitions.getCommandConcatenator();
        String[] split = input.split( Pattern.quote( concatenator ) ); //split into command calls
        CommandResult lastResult = null;
        BiFunction<String, VariableHandler, DataPair<String[][], String>> singleCommandHandler
                = stringResolvingMagic.singleCommandHandler(); //command string magic resolver
        implicitData = implicitData.addValue( CommandAPIConstants.ORIGINAL_INPUT, input );
        String originalInput = input;
        for ( String cmd : split )
        {
            if ( originalInput.length() > cmd.length() )
                originalInput = originalInput.substring( cmd.length() + 1 );
            implicitData = implicitData.addValue( CommandAPIConstants.ORIGINAL_WITHOUT_PROCESSED, originalInput );
            cmd = cmd.trim();
            //resolve commandline to its parts/args and the result variable
            DataPair<String[][], String> dataPair = singleCommandHandler.apply( cmd, handler );
            String[][] pair = dataPair.getA();
            //transform the arguments (without the commandname) to another format
            List<DataPair<String, String>> argumentList = Arrays.asList( pair ).subList( 1, pair.length ).stream()
                    .map( s -> s.length == 2 ? new DataPair<>( s[0].substring( definitions.getArgumentTagMarker().length() ), s[1] ) : new DataPair<>( (String) null, s[0] ) )
                    .collect( Collectors.toList() );
            String commandName = pair[0][0];
            if ( !commandName.startsWith( definitions.getCommandMarker() ) )
            {
                return CommandResult.create( null, ECommandResult.SYNTAX_EXCEPTION, "Invalid command. Command marker \""
                        + definitions.getCommandMarker() + "\" is missing." );
            }
            else commandName = commandName.substring( definitions.getCommandMarker().length() );
            DataPair<ResolvedCommand<?>, List<DataPair<String, String>>> commandData = findCommand( this.commands, commandName, argumentList );
            if ( execute ) // only execute if defined by input
            {
                lastResult = callCommand( commandData.getA(), commandData.getB(),
                        implicitData.addValue( CommandAPIConstants.ORIGINAL_COMMANDLINE, cmd ), handler, dataPair.getB() ); //implicit value ORIGINAL_COMMANDLINE is added only for this call
                if ( !lastResult.wasSuccessful() )
                {
                    return CommandResult.create( lastResult.get(), lastResult.getResult(), "Execution of \"" + cmd + "\" failed - " + lastResult.getResultMessage() );
                }
            }
            else
            {
                lastResult = resolveCommand( commandData.getA(), commandData.getB(),
                        implicitData.addValue( CommandAPIConstants.ORIGINAL_COMMANDLINE, cmd ), handler, dataPair.getB() );
            }
        }
        return lastResult;
    }

    private boolean validateCommands( Set<ResolvedCommand<?>> commands )
    {
        //check for missing types and duplicate parameter names
        for ( ResolvedCommand<?> cmd : commands )
        {
            validateName( cmd.getName(), "Command: " + cmd.getName() );
            //validate juniors
            if ( cmd.hasJuniorsCommands() )
            {
                Set<ResolvedCommand<?>> juniors = cmd.getJuniors();
                if ( !validateCommands( juniors ) ) return false;
            }
            if ( cmd.getResultingType() == null )
            {
                throw new UnknownTypeException( "The resulting type of the command \""
                        + cmd.getName() + "\" is unknown to the processor" );
            }
            //validate there are no duplicate names
            Set<String> parameterNames = new HashSet<>();
            for ( Parameter param : cmd.getParameters() )
            {
                String paramName = param.name();
                if ( param.type() == null )
                {
                    throw new UnknownTypeException( "The parameter type of \""
                            + paramName + "\" used in the command \""
                            + cmd.getName() + "\" is unknown to the processor" );
                }
                if ( !parameterNames.contains( paramName ) )
                {
                    validateName( paramName, "Command: " + cmd.getName() + ", parameter: " + paramName );
                    parameterNames.add( paramName );
                }
                else
                    throw new DuplicateNamesException( "Two parameters of command \"" + cmd.getName() + "\" have an identical name: \"" + paramName + "\". This might lead to unexpected errors." );
            }
        }
        return true;
    }

    private void validateName( String name, String message )
    {
        boolean invalid = name.startsWith( definitions.getTypeCastMarker() );
        invalid |= name.startsWith( definitions.getDeepAccessMarker() );
        invalid |= name.startsWith( definitions.getAbstractAccessMarker() );
        invalid |= name.startsWith( definitions.getVariableMarker() );
        invalid |= name.startsWith( definitions.getCommandMarker() );
        invalid |= name.startsWith( definitions.getArgumentTagMarker() );
        invalid |= name.startsWith( definitions.getResultingMarker() );
        invalid |= name.startsWith( definitions.getCommandConcatenator() );
        invalid |= name.startsWith( definitions.getStringConstantParentheses() );
        if ( invalid )
            throw new VariableSyntaxException( message + "; Invalid name: " + name );
    }

    //puts the arguments at the right order considering optionals as processed latest to detect missing arguments earlier

    private <T> CommandResult resolveArgumentPositioning( ResolvedCommand<T> command, List<DataPair<String, String>> arguments, ImplicitData implicitData )
    {
        //TODO
        ArrayList<Parameter> parameters = command.getParameters();
        String[] argsPositioning = new String[parameters.size()];
        List<DataPair<String, String>> fix = arguments.stream().filter( d -> d.getA() == null ).collect( Collectors.toList() );
        List<DataPair<String, String>> optional = arguments.stream().filter( d -> d.getA() != null ).collect( Collectors.toList() );
        Iterator<DataPair<String, String>> fixIterator = fix.iterator();
        Parameter lastUsed;
        CommandResult result = null;
        //process non optional parameters
        for ( int p = 0; p < parameters.size(); p++ )
        {
            Parameter parameter = parameters.get( p );
            if ( !parameter.isOptional() )
            {
                if ( !parameter.hasScope() )
                {
                    lastUsed = parameter;
                    if ( !fixIterator.hasNext() )
                    {
                        result = CommandResult.create( lastUsed, ECommandResult.INVALID_ARGUMENTS,
                                "Missing arguments, first missing: \"" + parameter.name() + "\" for command \"" + command.getName() + "\"" );
                        argsPositioning[p] = null;
                        break;
                    }
                    argsPositioning[p] = fixIterator.next().getB();
                }
                else
                {
                    argsPositioning[p] = implicitData.implicitData( parameter.scope() );
                }
            }
        }
        //put optional parameter at their place
        for ( int i = 0; i < optional.size(); i++ )
        {
            DataPair<String, String> arg = optional.get( i );
            for ( int p = 0; p < parameters.size(); p++ )
            {
                Parameter parameter = parameters.get( p );
                if ( !parameter.isOptional() ) continue;
                if ( !parameter.name().equals( arg.getA() ) ) break;
                argsPositioning[p] = arg.getB();
                break;
            }
        }
        return result != null ? result : CommandResult.create( argsPositioning, ECommandResult.SUCCESS, "arguments positioning done: " + command.getName() );
    }

    private <T> CommandResult argumentPositioning( ResolvedCommand<T> command, List<DataPair<String, String>> arguments, ImplicitData implicitData )
    {
        ArrayList<Parameter> parameters = command.getParameters();
        String[] argsPositioning = new String[parameters.size()];
        List<DataPair<String, String>> fix = arguments.stream().filter( d -> d.getA() == null ).collect( Collectors.toList() );
        List<DataPair<String, String>> optional = arguments.stream().filter( d -> d.getA() != null ).collect( Collectors.toList() );
        Iterator<DataPair<String, String>> fixIterator = fix.iterator();
        //process non optional parameters
        for ( int p = 0; p < parameters.size(); p++ )
        {
            Parameter parameter = parameters.get( p );
            if ( !parameter.isOptional() )
            {
                if ( !parameter.hasScope() )
                {
                    if ( !fixIterator.hasNext() )
                        return CommandResult.create( new DataPair<>( parameter, parameters ), ECommandResult.INVALID_ARGUMENTS,
                                "Missing arguments, first missing: \"" + parameter.name() + "\" for command \"" + command.getName() + "\"" );
                    argsPositioning[p] = fixIterator.next().getB();
                }
                else
                {
                    argsPositioning[p] = implicitData.implicitData( parameter.scope() );
                }
            }
        }
        //put optional parameter at their place
        for ( int i = 0; i < optional.size(); i++ )
        {
            DataPair<String, String> arg = optional.get( i );
            for ( int p = 0; p < parameters.size(); p++ )
            {
                Parameter parameter = parameters.get( p );
                if ( !parameter.isOptional() ) continue;
                if ( !parameter.name().equals( arg.getA() ) ) break;
                argsPositioning[p] = arg.getB();
                break;
            }
        }
        return CommandResult.create( argsPositioning, ECommandResult.SUCCESS, "arguments positioning done: " + command.getName() );
    }

    private <T> CommandResult argsConversion( String[] args, ResolvedCommand<T> command, VariableHandler handler )
    {
        Object[] converted = new Object[args.length];
        Type<String> stringType = typeContainer.getType( String.class );
        for ( int i = 0; i < args.length; i++ )
        {
            String arg = args[i];
            Parameter parameter = command.getParameters().get( i );
            if ( arg == null && !parameter.isOptional() )
                return CommandResult.create( null, ECommandResult.INVALID_ARGUMENTS, "Parameter \"" + parameter.name() + "\" is empty/null" );
            if ( arg != null && arg.startsWith( definitions.getVariableMarker() ) )
            {
                Type<?> type = parameter.type();
                arg = arg.substring( definitions.getVariableMarker().length() );
                CommandResult result = resolveVariable( type, arg, handler );
                if ( result.wasSuccessful() )
                    converted[i] = result.get();
                else return result;
            }
            else converted[i] = parameter.type().deepResolve( stringType, arg, x -> false );
        }
        return CommandResult.create( converted, ECommandResult.SUCCESS, "arg converted: " + Arrays.toString( args ) );
    }

    private <T> CommandResult resolveVariable( Type<T> type, String variableSyntax, VariableHandler handler )
    {
        //example: name?.property??
        String abstractAccessMarker = definitions.getAbstractAccessMarker(); //type access
        String deepAccessMarker = definitions.getDeepAccessMarker(); //property access
        String typeCastMarker = definitions.getTypeCastMarker(); //type cast
        String variableName = "";
        FunctionStack<Boolean, ?> stack;
        while ( !( variableSyntax.isEmpty() || variableSyntax.startsWith( abstractAccessMarker ) || variableSyntax.startsWith( deepAccessMarker )
                || variableSyntax.startsWith( typeCastMarker ) ) )
        {
            variableName += variableSyntax.charAt( 0 );
            variableSyntax = variableSyntax.substring( 1 );
        }
        Variable<?> variable = handler.getVariable( variableName );
        if ( variable == null )
            throw new VariableSyntaxException( "Unknown variable, no variable found by name \"" + variableName + "\"" );
        stack = x -> variable.getValue(); //basis stack
        stack = createVariableResolverStack( stack, null, variableSyntax ); //resolve complete syntax recursively
        if ( stack == null )
            return CommandResult.create( null, ECommandResult.SYNTAX_EXCEPTION, "" );
        FunctionStack<Boolean, T> formattedStack = stack.format( typeContainer, type );
        T result = formattedStack.apply( true );
        if ( result == null )
            return CommandResult.create( null, ECommandResult.SYNTAX_EXCEPTION, "Result produced by variable syntax \"" + variableSyntax + "\" is null" );
        return CommandResult.create( result, ECommandResult.SUCCESS, "variable resolved: " + variableName );
    }

    private <T> FunctionStack<T, ?> createVariableResolverStack( FunctionStack<T, ?> basis, String nextStep, String backlog )
    {
        if ( basis == null ) return null;
        if ( nextStep == null )
        {
            //resolving next step
            if ( backlog == null || backlog.isEmpty() ) return basis;
            if ( backlog.startsWith( definitions.getAbstractAccessMarker() ) )
            {
                backlog = backlog.substring( 1 );
                return createVariableResolverStack( basis, definitions.getAbstractAccessMarker(), backlog.isEmpty() ? null : backlog );
            }
            else if ( backlog.startsWith( definitions.getDeepAccessMarker() ) )
            {
                String step = definitions.getDeepAccessMarker();
                backlog = backlog.substring( definitions.getDeepAccessMarker().length() );
                while ( !( backlog.startsWith( definitions.getDeepAccessMarker() ) || backlog.startsWith( definitions.getAbstractAccessMarker() )
                        || backlog.startsWith( definitions.getTypeCastMarker() ) ) )
                {
                    if ( backlog.isEmpty() ) break;
                    step += backlog.charAt( 0 );
                    backlog = backlog.substring( 1 );
                }
                return createVariableResolverStack( basis, step, backlog.isEmpty() ? null : backlog );
            }
            else if ( backlog.startsWith( definitions.getTypeCastMarker() ) )
            {
                String step = definitions.getTypeCastMarker();
                backlog = backlog.substring( definitions.getTypeCastMarker().length() );
                while ( !( backlog.startsWith( definitions.getDeepAccessMarker() ) || backlog.startsWith( definitions.getAbstractAccessMarker() )
                        || backlog.startsWith( definitions.getTypeCastMarker() ) ) )
                {
                    if ( backlog.isEmpty() ) break;
                    step += backlog.charAt( 0 );
                    backlog = backlog.substring( 1 );
                }
                return createVariableResolverStack( basis, step, backlog.isEmpty() ? null : backlog );
            }
            else
                throw new VariableSyntaxException( "Wrong variable syntax; error occurred while processing \"" + backlog + "\" as ending of the variable -> marker is missing" );
        }
        else if ( nextStep.startsWith( definitions.getAbstractAccessMarker() ) )
        {
            //abstract to type of current value
            basis = basis.abstraction( typeContainer );
            return createVariableResolverStack( basis, null, backlog );
        }
        else if ( nextStep.startsWith( definitions.getDeepAccessMarker() ) )
        {
            //access property
            String property = nextStep.substring( definitions.getDeepAccessMarker().length() );
            if ( property.isEmpty() )
                throw new VariableSyntaxException( "Wrong variable syntax; error occurred while processing \"" + nextStep + "\" as part of the variable -> expected property name is missing" );
            basis = basis.access( typeContainer, property );
            return createVariableResolverStack( basis, null, backlog );
        }
        else if ( nextStep.startsWith( definitions.getTypeCastMarker() ) )
        {
            //cast to type
            String type = nextStep.substring( definitions.getTypeCastMarker().length() );
            if ( !typeContainer.hasType( type ) )
                throw new UnknownTypeException( "There is no type for name \"" + type + "\"" );
            basis = basis.format( typeContainer, typeContainer.getType( type ) );
            return createVariableResolverStack( basis, null, backlog );
        }
        else
        {
            throw new VariableSyntaxException( "Wrong variable syntax; error occurred while processing \"" + nextStep + "\" as part of the variable -> marker is missing" );
        }
    }

    private <T> CommandResult resolveCommand( ResolvedCommand<T> command, List<DataPair<String, String>> arguments, ImplicitData implicitData, VariableHandler handler, String resultVariable )
    {
        if ( command.hasPermission() && !implicitData.isExecutionPermitted( command.getPermission() ) )
            return CommandResult.create( null, ECommandResult.UNPERMITTED, "The execution is not permitted. Permission: " + command.getPermission() );
        //add implicit value ORIGINAL_COMMAND_NAME
        implicitData = implicitData.addValue( CommandAPIConstants.ORIGINAL_COMMAND_NAME, command.getName() );
        CommandResult result = resolveArgumentPositioning( command, arguments, implicitData ); //TODO
        if ( !result.wasSuccessful() ) return result;
        String[] argsPositioning = (String[]) result.get();
        result = argsConversion( argsPositioning, command, handler );
        return result;
    }

    //main command call will be here
    private <T> CommandResult callCommand( ResolvedCommand<T> command, List<DataPair<String, String>> arguments, ImplicitData implicitData, VariableHandler handler, String resultVariable )
    {
        CommandResult result = resolveCommand( command, arguments, implicitData, handler, resultVariable );
        if ( !result.wasSuccessful() ) return result;
        Object[] argumentData = (Object[]) result.get();
        T methodResult = command.getFunction().apply( argumentData );
        if ( resultVariable != null && !resultVariable.isEmpty() )
        {
            handler.introduceVariable( resultVariable, command.getResultingType(), methodResult );
            return CommandResult.create( null, ECommandResult.SUCCESS, "Variable \"" + resultVariable + "\"created" );
        }
        else
            return CommandResult.create( methodResult, ECommandResult.SUCCESS, "Command call complete: " + command.getName() );
    }

    //find command matching the name and possible sub-args
    private DataPair<ResolvedCommand<?>, List<DataPair<String, String>>> findCommand( Set<ResolvedCommand<?>> commands, String name, List<DataPair<String, String>> possibleSubs )
    {
        if ( commands == null ) return null;
        Optional<ResolvedCommand<?>> first = commands.stream().filter( cmd -> cmd.getName().equals( name ) ).findFirst();
        if ( first.isPresent() )
        {
            ResolvedCommand<?> resolvedCommand = first.get();
            if ( possibleSubs.size() > 0 && possibleSubs.get( 0 ).getA() == null )
            {
                DataPair<ResolvedCommand<?>, List<DataPair<String, String>>> result =
                        findCommand( resolvedCommand.getJuniors(), possibleSubs.get( 0 ).getB(), possibleSubs.subList( 1, possibleSubs.size() ) );
                if ( result != null )
                    return result;
            }
            return new DataPair<>( resolvedCommand, possibleSubs );
        }
        else return null;
    }

}

/***********************************************************************************************
 *
 *                  All rights reserved, SpaceParrots UG (c) copyright 2018
 *
 ***********************************************************************************************/