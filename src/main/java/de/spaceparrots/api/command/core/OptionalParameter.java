package de.spaceparrots.api.command.core;

import de.spaceparrots.api.command.interfaces.Parameter;
import de.spaceparrots.api.type.Type;

/**
 * <h1>CommandAPI Remastered</h1>
 *
 * @author Julius Korweck
 * @version 05.12.2018
 * @since 05.12.2018
 */
final class OptionalParameter implements Parameter
{

    private final Type<?> type;
    private final String name;

    OptionalParameter( Type<?> type, String name )
    {
        this.type = type;
        this.name = name;
    }

    @Override
    public Type<?> type()
    {
        return type;
    }

    @Override
    public String name()
    {
        return name;
    }

    @Override
    public boolean isOptional()
    {
        return true;
    }

    @Override
    public int scope()
    {
        return SCOPE_DEFAULT;
    }

    @Override
    public boolean hasScope()
    {
        return false;
    }
}

/***********************************************************************************************
 *
 *                  All rights reserved, SpaceParrots UG (c) copyright 2018
 *
 ***********************************************************************************************/