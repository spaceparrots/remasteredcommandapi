package de.spaceparrots.api.command.core;

import de.spaceparrots.api.command.annotation.*;
import de.spaceparrots.api.command.interfaces.InstanceSupplier;
import de.spaceparrots.api.command.interfaces.Parameter;
import de.spaceparrots.api.type.Type;
import de.spaceparrots.api.type.TypeContainer;

import java.lang.reflect.Method;
import java.util.*;
import java.util.stream.Collectors;

import static de.spaceparrots.api.command.interfaces.Parameter.SCOPE_DEFAULT;

/**
 * <h1>CommandAPI Remastered</h1>
 *
 * @author Julius Korweck
 * @version 06.12.2018
 * @since 03.12.2018
 */
class CommandResolver
{

    private TypeContainer typeContainer;
    private InstanceSupplier supplier;

    CommandResolver( TypeContainer container, InstanceSupplier supplier )
    {
        this.typeContainer = container;
        this.supplier = supplier;
    }

    private Set<ResolvedCommand<?>> resolveCommands( Class<?> executionClass, Method method )
    {
        //convert class to instance using supplier
        Object executionBody = supplier.produce( executionClass );

        return Arrays.stream( method.getAnnotationsByType( Command.class ) )
                .map( cmd -> mapCommand( cmd, method, new CommandExecution<>( executionBody, method ) ) )
                .collect( Collectors.toSet() );
    }

    private <T> ResolvedCommand<T> mapCommand( Command command, Method method, CommandExecution<T> execution )
    {
        Class<T> returnType = (Class<T>) method.getReturnType();
        List<Parameter> parameters = Arrays.stream( method.getParameters() )
                .map( this::mapParameter )
                .collect( Collectors.toList() );
        Type<T> returningType = typeContainer.getType( returnType );
        if ( returningType == null && method.getGenericReturnType().getTypeName().equals( "void" ) )
            returningType = (Type<T>) typeContainer.getType( "void" );
        ResolvedCommand<T> result = null;
        if ( method.isAnnotationPresent( Junior.class ) )
        {
            Set<ResolvedCommand<?>> juniors = Arrays.stream( method.getAnnotationsByType( Junior.class ) )
                    .map( Junior::value ) //receive all junior classes
                    .filter( clazz -> !clazz.isAnnotationPresent( Senior.class ) ) //all senior classes are invalid for beeing Junior themself
                    .map( this::resolveCommands )
                    .flatMap( Collection::stream )
                    .collect( Collectors.toSet() );
            result = new ResolvedCommand<>( command.value(), returningType, juniors, (ArrayList<Parameter>) parameters, execution );
        }
        else
            result = new ResolvedCommand<>( command.value(), returningType, (ArrayList<Parameter>) parameters, execution );
        if ( method.isAnnotationPresent( Permission.class ) )
        {
            Permission annotation = method.getAnnotation( Permission.class );
            result.setPermission( annotation.value() );
        }
        return result;
    }

    private Parameter mapParameter( java.lang.reflect.Parameter parameter )
    {
        String name = parameter.getName();
        Type<?> type = typeContainer.getType( parameter.getType() ); //resolve the type of the parameter
        Scope scope = parameter.getAnnotation( Scope.class );
        if ( parameter.isAnnotationPresent( Option.class ) )
        {
            //if Opt is present, its an optional parameter
            name = parameter.getAnnotation( Option.class ).value();
            return new OptionalParameter( type, name );
        }
        else if ( parameter.isAnnotationPresent( Arg.class ) )
        {
            //Arg only overwrites the name of the parameter
            name = parameter.getAnnotation( Arg.class ).value();
        }
        int scopeValue = scope != null ? scope.value() : SCOPE_DEFAULT;
        return new StandardParameter( type, name, scopeValue, scope != null );
    }

    private Set<Method> filterCommandMethods( Class<?> commandClass )
    {
        Method[] declaredMethods = commandClass.getDeclaredMethods();
        return Arrays.stream( declaredMethods )
                .filter( method -> method.isAnnotationPresent( Command.class ) || method.isAnnotationPresent( Commands.class ) ) //filter methods, that are tagged to be commands
                .collect( Collectors.toSet() ); //collect methods to set since there is no need for an order
    }

    Set<ResolvedCommand<?>> resolveCommands( Class<?> commandClass )
    {
        return filterCommandMethods( commandClass ).stream()
                .map( cmd -> resolveCommands( commandClass, cmd ) ) //resolve commands for method
                .flatMap( Collection::stream ) //make them to single set
                .collect( Collectors.toSet() );
    }

}

/***********************************************************************************************
 *
 *                  All rights reserved, SpaceParrots UG (c) copyright 2018
 *
 ***********************************************************************************************/